import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import App from './components/App';
import configureStore from './store'
import { createBrowserHistory } from "history";
import { ConnectedRouter } from 'connected-react-router';
import { Route, Switch, Redirect } from "react-router";

const history = createBrowserHistory();
const store = configureStore(history);
ReactDOM.render(
   <Provider store={store}>
       <ConnectedRouter history={history}>
           <Switch>
               <Route exact path="/" component={App} />
               <Redirect to="/"/>
           </Switch>
       </ConnectedRouter>
   </Provider>,
    document.getElementById('root'));

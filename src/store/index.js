import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import {createLogger} from 'redux-logger';
import createSagaMiddleware from 'redux-saga'
import createRootReducer from './reducers'
import sagas from './sagas'

export default function configureStore(history, services = {}) {
    const logger = createLogger({
        diff: true,
        collapsed: true
    });
    const sagaMiddleware = createSagaMiddleware();
    const middleware = applyMiddleware(sagaMiddleware, logger);

    const store = createStore(
        createRootReducer(history),
        composeWithDevTools(middleware),
    );

    let sagaTask = sagaMiddleware.run(sagas, services);

    if (module.hot) {
        module.hot.accept('./reducers', () => {
            const nextReducer = require('./reducers').default;
            store.replaceReducer(nextReducer)
        });
        module.hot.accept('./sagas', () => {
            const nextSagas = require('./sagas').default;
            sagaTask.cancel();
            sagaTask.done.then(() => {
                sagaTask = sagaMiddleware.run(nextSagas, services)
            })
        })
    }
    return store
}